# CimaKeep Project

This is a project that combines a Quasar v2 frontend with a Laravel backend. Here are the instructions for setting it up:

## Frontend

1. Install Node.js version 18.5 or higher.
2. Navigate to the root directory of the project and run `npm install` to install the project's dependencies.
3. Run `quasar dev` to start the development server.

## Backend

1. Install PHP version 8.1 or higher.
2. Navigate to the `api` folder and run `composer install` to install the project's dependencies.
3. Migrate the database by running `php artisan migrate`.
4. Start the backend server by running `php artisan serve`.
5. The project comes with a pre-configured `.env` file.

## Configuration

The API url can be configured in `CimaKeep/src/config.js`. Edit the `apiUrl` variable to change the url of the Laravel backend.

## Note

Make sure to run both the frontend and backend servers at the same time for the project to work properly.

## Dependencies

- Quasar v2
- Laravel 9
- Composer
- Node.js
- npm

Feel free to contact me if you have any questions or concerns!
