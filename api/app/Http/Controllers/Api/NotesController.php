<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\NoteRequestValidate;
use App\Models\Note;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;


class NotesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        $notes = Note::orderByDesc('has_pin')->orderByDesc('id')->where('is_archived', 0)->paginate(20);

        return response()->json([
            'notes' => $notes->items(),
            'pagination' => [
                'current_page' => $notes->currentPage(),
                'last_page' => $notes->lastPage(),
                'prev_page_url' => $notes->previousPageUrl(),
                'next_page_url' => $notes->nextPageUrl(),
            ]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param NoteRequestValidate $request
     * @return JsonResponse
     *
     */
    public function store(NoteRequestValidate $request)
    {
        Note::create($request->all(
            'title',
            'content',
            'has_pin',
            'is_archived',
        ));
        return response()->json([
            'data' => $request->all()
        ], 201);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param NoteRequestValidate $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(NoteRequestValidate $request, $id)
    {
        Note::find($id)->update($request->all('title', 'content', 'has_pin', 'is_archived'));

        return response()->json([
            'message' => 'success'
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        Note::find($id)->delete();

        return response()->json([
            'message' => 'success'
        ], 204);
    }


    /**
     * Changes the pin status of a specific note
     *
     * @param int $id
     * @return JsonResponse
     */

    public function archive($id, Request $request)
    {
        Note::find($id)->update($request->all('has_pin', 'is_archived'));

        return response()->json([
            'message' => 'success'
        ], 200);
    }

    /**
     * Show the archived notes.
     * @return JsonResponse
     */

    public function archived()
    {
        $notes =  Note::orderByDesc('id')->where('is_archived', 1)->paginate(20);

        return response()->json([
            'notes' => $notes->items(),
            'pagination' => [
                'current_page' => $notes->currentPage(),
                'last_page' => $notes->lastPage(),
                'prev_page_url' => $notes->previousPageUrl(),
                'next_page_url' => $notes->nextPageUrl(),
            ]
        ]);
    }
}
