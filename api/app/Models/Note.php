<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Note extends Model
{
    use HasFactory;

    protected $guarded = ['id'];
    protected $appends = ['ShortestContent'];

    public function getShortestContentAttribute()
    {
        $content = Str::limit($this->content, 70);
        $title = Str::limit($this->title, 12);


        return [
            'content' => $content,
            'title' => $title,
        ];
    }
}
