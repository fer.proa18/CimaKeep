<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\Api\NotesController;

/*Get the archived notes */
Route::get('notes/archived', [NotesController::class,'archived']);
/*Routes for the creation, update and delete notes*/
Route::resource('notes', NotesController::class)->except(['show','edit']);
/* Archive notes routes */
Route::post('notes/{id}/archive', [NotesController::class,'archive']);

